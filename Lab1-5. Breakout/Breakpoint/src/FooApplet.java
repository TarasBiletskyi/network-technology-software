import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.*;
 
import javax.swing.*;
 
@SuppressWarnings("serial")
public class FooApplet extends JApplet {
   private TitlePanel titleView;
   private ContentPanel contentView;
   private CardLayout cardlayout = new CardLayout();
 
   public void init() {
      setLayout(cardlayout);
       
      ActionListener buttonListener = new ActionListener() {
          
         @Override
         public void actionPerformed(ActionEvent arg0) {
            cardlayout.next(getContentPane());
            //contentView.run();
         }
      };
 
      titleView = new TitlePanel();
      titleView.addNextActionListener(buttonListener);
      contentView = new ContentPanel();
      contentView.addNextActionListener(buttonListener);
 
      add(titleView, titleView.getName());
      add(contentView, contentView.getName());
 
   }
}
 
@SuppressWarnings("serial")
class TitlePanel extends BasePanel {
   public static final String NAME = "Title Panel";
 
   public TitlePanel() {
      super(NAME);
   }
 
}
 
@SuppressWarnings("serial")
class ContentPanel extends BasePanel {
   public static final String NAME = "Content Panel";
   public Breakpoint br;
  
   public ContentPanel() {   
     super(NAME);
   }
   
//   public void run()
//   {
//	   br = new Breakpoint();
//	   br.run();
//   }
}
 
abstract class BasePanel extends JPanel {
   private static final float FONT_SIZE = 24f;
   private JButton next = new JButton("Next");
 
   public BasePanel(String name) {
      setName(name);
      JLabel label = new JLabel(getName(), SwingConstants.CENTER);
      label.setFont(label.getFont().deriveFont(Font.BOLD, FONT_SIZE));
 
      JPanel btnPanel = new JPanel(new GridBagLayout());
      btnPanel.add(next);
 
      setLayout(new BorderLayout());
      add(btnPanel, BorderLayout.SOUTH);
      add(label, BorderLayout.CENTER);
   }
 
   public void addNextActionListener(ActionListener listener) {
      next.addActionListener(listener);
   }
}