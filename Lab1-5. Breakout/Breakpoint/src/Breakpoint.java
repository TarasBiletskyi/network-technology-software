import acm.graphics.*;
import acm.program.*;
import acm.util.*;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.*;


interface CONSTANTS
{
	//public static int BRICK_COUNT = 100;
	
	public static final int APPLICATION_WIDTH = 400;
	public static final int APPLICATION_HEIGHT = 800;

	/** ������ ����� */
	public static final int WIDTH = APPLICATION_WIDTH;
	//public static final int HEIGHT = APPLICATION_HEIGHT;
	
	public static final int PADDLE_WIDTH = 60;
	public static final int PADDLE_HEIGHT = 10;

	public static final int PADDLE_Y_OFFSET = 30;

	public static final int NBRICKS_PER_ROW = 10;

	public static final int NBRICK_ROWS = 10;

	public static final int BRICK_SEP = 4;

	public static final int BRICK_WIDTH = (WIDTH - (NBRICKS_PER_ROW - 1) * BRICK_SEP) / NBRICKS_PER_ROW;

	public static final int BRICK_HEIGHT = 8;

	public static final int BALL_RADIUS = 10;

	public static final int BRICK_Y_OFFSET = 300;

	public static final int NTURNS = 3;
	
	/**��������� ��������� ����� ��� vx*/	
	public RandomGenerator rgen = RandomGenerator.getInstance();
	
	/** �������� �� ������ ����� �'��� */	
	public static final int DELAY = 10;
}

public class Breakpoint extends GraphicsProgram implements CONSTANTS, Runnable
{
	private static final long serialVersionUID = 1L;

	Thread MoveBallThreadObj = null;
	Thread MoveServerBallThreadObj = null;
	
   static JLabel BestResultLbl;
   static JLabel CurrentResultLbl;
   static JTextField NickNametxt;
   static int BestScoreResult = 1000;
   public static int CurrentScoreResult = 0;
   static String PlayerNickName = null;
   
    public Breakpoint()
    {
    	InitPanel();
    }
    private void InitPanel()
    {
        this.add(getCustPanel());  
    }
    private JPanel getCustPanel() 
    {
        JPanel panel = new JPanel ();
        panel.setLayout (new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        JLabel addLabel1 = addLabel("Your Nickname:", panel);
        
        NickNametxt =  addTextBox("", panel);
 
        addButton ("Start", panel);
        
        BestResultLbl = addLabel("Your best result: ", panel);
        CurrentResultLbl = addLabel("Your current result: 0", panel);
        
        return panel;
    }
    public static void StoreBestResult(int Score) throws UnknownHostException, IOException
    {
    	Client.StoreBestScore(Score);
    }
    
    public static void StoreNewPlayer(String Nickname) throws UnknownHostException, IOException
    {
    	Client.StoreNewPlayer(Nickname);
    }
    
    public static void SetBestResult(String NickName) throws UnknownHostException, IOException, ClassNotFoundException
    {
    	BestScoreResult = Client.GetLastBestScore(NickName);   	
    	BestResultLbl.setText(String.format("Your best result: %d", BestScoreResult));
    }
    
    public static void SetCurrentResult(int count)
    {
    	CurrentResultLbl.setText(String.format("Your current result: %d", count));
    }
    
    private void addButton (String text, JPanel container)
    {
        JButton button = new JButton (text);
        
        button.setMinimumSize(new Dimension(500,20));
        button.setAlignmentX (CENTER_ALIGNMENT);
        button.setFont(new Font("Serif", Font.BOLD, 20));
        
        container.add(button);
        
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {           	
            	try {
            		PlayerNickName = NickNametxt.getText();
					SetBestResult(PlayerNickName);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }

        });
    }
    
    private JTextField addTextBox (String text, JPanel container)
    {
    	JTextField textField = new JTextField(10);
    	textField.setMaximumSize(new Dimension(500,30));
    	textField.setMinimumSize(textField.getPreferredSize());
    	textField.setFont(new Font("Serif", Font.BOLD, 20));
    	
        container.add (textField/*,right*/);
        
        return textField;
    }
    
    private JLabel addLabel(String text, JPanel container)
    {
    	JLabel label = new JLabel(text);
    	label.setAlignmentX(Component.CENTER_ALIGNMENT);
    	label.setFont(new Font("Serif", Font.BOLD, 20));
    	container.add(label);
    	
    	return label;
    }
    
    private ActionListener StartButtonActionListener()
    {
        ActionListener buttonListener = new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent arg0) {
               //cardlayout.next(getContentPane());
               //contentView.run();
            }
         };
           
         return buttonListener;
    }
     
/* Method: run() */
/* Runs the Breakout program. */
	public void run() 
	{	
		try 
		{
			//System.out.println("start");
			Client.startMain();
			StartPlayGame();			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}	
	}
		
	public void StartPlayGame() throws UnknownHostException, IOException, ClassNotFoundException
	{
		
		for(int i=0; i < NTURNS; i++) 
		{
			setUpGame();
			
			playGame();
			
			if(MoveBallThreadObj != null)
				while(MoveBallThreadObj.isAlive()){}

			if(brickCounter == 0) 
			{
				ball1.setVisible(false);
				ServerBall1.setVisible(false);
				
				printWinner();
				MoveBallThreadObj.interrupt();
				MoveServerBallThreadObj.interrupt();
				MoveServerBallThreadObj = null;
				MoveBallThreadObj = null;
				break;
			}
			
			if(brickCounter > 0) 
			{
				if(CurrentScoreResult > BestScoreResult)
				{
					StoreBestResult(CurrentScoreResult);
					SetBestResult(PlayerNickName);
				}
				
				MoveServerBallThreadObj.interrupt();
				MoveBallThreadObj.interrupt();
				
				MoveServerBallThreadObj = null;
				MoveBallThreadObj = null;
				removeAll();				 
			}
		}
		
		if(brickCounter > 0)
			printGameOver();
		
	}
	
	static public Ball ball1;	
	static public Ball ServerBall1;
	
	private void setUpGame() {
		ball1.brickCounter = 100;
		//brickCounter = 100;
		drawBricks( APPLICATION_WIDTH/2, BRICK_Y_OFFSET);
		drawPaddle();
		drawBall();
	}

	//��������� �������������� ��'���� �������
	private GRect brick;
	
	//��������� ���������� ������
	private void drawBricks(double cx, double cy) {				
		
		brickCounter = NBRICK_ROWS * NBRICKS_PER_ROW;
		
		for( int row = 0; row < NBRICK_ROWS; row++ ) 
		{
			
			for (int column = 0; column < NBRICKS_PER_ROW; column++) 
			{
				
				/* ��� �������� ���������� x ������� ����������� �� ������ �� ������ ����, 
				   �������� �������� ������ ������ �� ������� � �����(�� �� �������) 
				   ��� ����� �������� ������� � ����� ������ �� ������ �� ������
				*/
				
				double	x = cx - (NBRICKS_PER_ROW*BRICK_WIDTH)/2 - ((NBRICKS_PER_ROW-1)*BRICK_SEP)/2 + column*BRICK_WIDTH + column*BRICK_SEP;
				
				/* ��� �������� ���������� y ����������� �� ������� ����� ������� �����,
				   ������ ������ ������� �� ��������� ������� ���������� ����
				 */
				
				double	y = cy + row*BRICK_HEIGHT + row*BRICK_SEP;

				brick = new GRect( x , y , BRICK_WIDTH , BRICK_HEIGHT );
				add (brick);
				brick.setFilled(true);
				
				//Setting colors depending on which row the bricks are in
				
				if (row < 2) {
					brick.setColor(Color.RED);
				}
				if (row == 2 || row == 3) {
					brick.setColor(Color.ORANGE);
				}
				if (row == 4 || row == 5) {
					brick.setColor(Color.YELLOW);
				}
				if (row == 6 || row == 7) {
					brick.setColor(Color.GREEN);
				}
				if (row == 8 || row == 9) {
					brick.setColor(Color.CYAN);
				}
			}
		}
	}
	
	//adding individual paddle object
	private GRect paddle;
	private GRect ServerPaddle;
	//paddle set-up
	private void drawPaddle() {
		//������� �� �����
		double x = APPLICATION_WIDTH/2 - PADDLE_WIDTH/2; 
		double y = APPLICATION_HEIGHT - PADDLE_Y_OFFSET - PADDLE_HEIGHT;
		
		double yServer =  PADDLE_Y_OFFSET - PADDLE_HEIGHT;
		
		ServerPaddle = new GRect (x, yServer, PADDLE_WIDTH, PADDLE_HEIGHT);
		paddle = new GRect (x, y, PADDLE_WIDTH, PADDLE_HEIGHT);
		
		paddle.setFilled(true);
		ServerPaddle.setFilled(true);
		
		add(ServerPaddle);
		add (paddle);
		addMouseListeners();
	}
	
	//������� ����� �� �����
	public void mouseMoved(MouseEvent e) {
		//³������������� ������� ������� ��������� � ���� �������, ���� ������� �� �������� �� ��� ����
		//���������� Y �������
		if ((e.getX() < APPLICATION_WIDTH - PADDLE_WIDTH/2) && (e.getX() > PADDLE_WIDTH/2)) {
			paddle.setLocation(e.getX() - PADDLE_WIDTH/2, APPLICATION_HEIGHT - PADDLE_Y_OFFSET - PADDLE_HEIGHT);
		}
	}

	//ball set-up
	private void drawBall() 
	{
		ServerBall1 = new Ball(APPLICATION_WIDTH/2 - BALL_RADIUS, APPLICATION_HEIGHT/3 - BALL_RADIUS, BALL_RADIUS, this);	
		ball1 = new Ball(APPLICATION_WIDTH/2 - BALL_RADIUS, APPLICATION_HEIGHT/3 * 2 - BALL_RADIUS, BALL_RADIUS, this);	
		
		ServerBall1.Paint();
		ball1.Paint();
		
		ServerBall1.ServerPaddle = ServerPaddle;
		ball1.paddle = paddle;
	}
	
	private void playGame() {
		waitForClick();
	
		if(MoveBallThreadObj == null)
		{
			MoveBallThreadObj = new MoveBallThread(ball1);	
			MoveBallThreadObj.start();
		}
		
		if(MoveServerBallThreadObj == null)
		{
			MoveServerBallThreadObj = new MoveBallThread(ServerBall1);
			MoveServerBallThreadObj.start();
		}
	
	}
	
	public void MoveBallTillEnd(Ball MyBall) throws UnknownHostException, IOException, ClassNotFoundException
	{
		while (true) {

			MyBall.moveBall();
			
			if(MyBall == ServerBall1)
			{
				
				double newServerBallX = Client.GetPositionYFromServer(MyBall.getX());
		
				ServerPaddle.move(newServerBallX - ServerPaddle.getX() - 20, 0);
			}
			
			if (MyBall.getY() >= APPLICATION_HEIGHT) {
				break;
			}
			
			if(brickCounter == 0) {
				break;
			}
		}
	}
	
	private void printGameOver() {
		GLabel gameOver = new GLabel ("Game Over", APPLICATION_WIDTH/2, APPLICATION_HEIGHT/2);
		gameOver.move(-gameOver.getWidth()/2, -gameOver.getHeight());
		gameOver.setColor(Color.RED);
		add (gameOver);
	}
	
	private int brickCounter = 100;
	
	private void printWinner() {
		GLabel Winner = new GLabel ("Winner!!", APPLICATION_WIDTH/2, APPLICATION_HEIGHT/2);
		Winner.move(-Winner.getWidth()/2, -Winner.getHeight());
		Winner.setColor(Color.RED);
		add (Winner);
	}
}	

class StartGameThread extends Thread implements Runnable
{
	Breakpoint applet = null;
	
	public StartGameThread(Breakpoint Passed_applet)
	{
		applet = (Breakpoint)Passed_applet;	
	}
	
	public void run()
	{
		try 
		{
			applet.StartPlayGame();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}	
}

class Ball extends GraphicsProgram implements CONSTANTS
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	GOval ball;	
	
	int UserScoreNumber = 0;
	
	double x, y;
	double vx, vy;
	int radius;

	Breakpoint applet = null;
	static GRect paddle = null;
	static GRect ServerPaddle = null;
	
	static int brickCounter = 100;
	//����������� 	
	public Ball(double passed_x, double passed_y, int passed_radius, Breakpoint Passed_applet)
	{
		applet = (Breakpoint)Passed_applet;

		x = passed_x;
		y = passed_y;
		radius = passed_radius;
		
		ball = new GOval(x, y, radius, radius);
		ball.setFilled(true);
		
		getBallVelocity();
	}
	
	public void Paint()
	{
		applet.add(ball);
		
	}
	
	public void move()
	{	
		ball.move(vx, vy);
	}
	
	private void getBallVelocity() 
	{
		vy = 4.0;
		//�������� ����� �������� �� 1 � 3
		vx = rgen.nextDouble(1.0, 3.0);
		
		//�������� ����� bool �������� � ���������� 50%
		if (rgen.nextBoolean(0.5)) {
			vx = -vx; 
		}
	
	}
	
	public int getX()
	{
		return (int)ball.getX();
	}
	
	public int getY()
	{
		return (int)ball.getY();
	}
	
	public void moveBall() {

		move();
		
		x = getX();
		y = getY();
		
		//�������� �� ����
		//need to get vx and vy at the point closest to 0 or the other edge
		if ((x - vx <= 0 && vx < 0 )||(x + vx >= (APPLICATION_WIDTH - BALL_RADIUS*2) && vx>0)) {
			vx *= -1;
		}
		//�������� �� �����
//		if ((y - vy <= 0 && vy < 0 )) {
//			vy *= -1;
//		}
		
		//�������� �� ǲ������� � ������ ��'������
		GObject collider = getCollidingObject();
		//vy= -0.5;
		
		 if (collider == ServerPaddle) 
		{
			//System.out.println("qwerty");
			//��������, �� �'�� ����������� � ������� �������� ��� ��������
			if(y >= /*PADDLE_Y_OFFSET + PADDLE_HEIGHT + BALL_RADIUS*2 + */20 /*&& vy < PADDLE_Y_OFFSET - PADDLE_HEIGHT - BALL_RADIUS*2 + 4*/) 
			{
				//System.out.println("qwerty2");
				vy *= -1;	
			}
		}
		
		 else if (collider == paddle) 
		{

			//��������, �� �'�� ����������� � ������� �������� ��� ��������
			if(y >= APPLICATION_HEIGHT - PADDLE_Y_OFFSET - PADDLE_HEIGHT - BALL_RADIUS*2 -9 && vy < APPLICATION_HEIGHT- PADDLE_Y_OFFSET - PADDLE_HEIGHT - BALL_RADIUS*2 + 4) 
			{
				vy *= -1;	
			}
		}
		
		//�������� �� ��������
		else if (collider != null && collider.getClass() != ball.getClass()) {
			applet.remove(collider); 
			if(this == Breakpoint.ball1)
			{
				UserScoreNumber++;
				brickCounter--;
			}
			Breakpoint.CurrentScoreResult = 100 - brickCounter;
			Breakpoint.SetCurrentResult(Breakpoint.CurrentScoreResult);

			vy *= -1;
		}
		
		applet.pause (DELAY);
	}
	
	
	private GObject getCollidingObject() {
		

		if((applet.getElementAt(x, y)) != null) {
	         return applet.getElementAt(x, y);
	      }
		else if (applet.getElementAt( (x + BALL_RADIUS*2), y) != null ){
	         return applet.getElementAt(x + BALL_RADIUS*2, y);
	      }
		else if(applet.getElementAt(x, (y + BALL_RADIUS*2)) != null ){
	         return applet.getElementAt(x, y + BALL_RADIUS*2);
	      }
		else if(applet.getElementAt((x + BALL_RADIUS*2), (y + BALL_RADIUS*2)) != null ){
	         return applet.getElementAt(x + BALL_RADIUS*2, y + BALL_RADIUS*2);
	      }
		//���� ���� ��'����
		else{
	         return null;
	      }
		
	}
}

class MoveBallThread extends Thread implements Runnable
{
	Ball ball;
	Breakpoint applet = null;
	
	public MoveBallThread(Ball Passed_ball)
	{
		ball = Passed_ball;
		this.applet = (Breakpoint)Passed_ball.applet;		
	}
	
	public void run()
	{
		try {
			applet.MoveBallTillEnd(ball);
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

}

class Client{
	//public static void main(String args[]) throws UnknownHostException, IOException
	
	private static Socket socket = null;
	private static ObjectOutputStream OutputObject = null;
	private static ObjectInputStream InputObject = null;
	
	public static void startMain() throws UnknownHostException, IOException
	{
		//System.out.println("Start Main");
	}
	public static void StoreNewPlayer(String Nickname) throws UnknownHostException, IOException
	{
		socket= new Socket("127.0.0.1", 1346);
		
		OutputObject = new ObjectOutputStream(socket.getOutputStream());
		OutputObject.writeObject(Nickname);
	}
	
	public static void StoreBestScore(int Score) throws UnknownHostException, IOException
	{
		socket= new Socket("127.0.0.1", 1346);
		
		OutputObject = new ObjectOutputStream(socket.getOutputStream());
		OutputObject.writeObject(Score);
	}
	
	public static int GetLastBestScore(String NickName) throws UnknownHostException, IOException, ClassNotFoundException
	{	
		socket = new Socket("127.0.0.1", 1346);
		
		OutputObject = new ObjectOutputStream(socket.getOutputStream());
		OutputObject.writeObject(NickName);
	
		InputObject = new ObjectInputStream(socket.getInputStream());
		int a = (int)InputObject.readObject();
	
		return a;
	}
	
	public static double GetPositionYFromServer(double inputX) throws UnknownHostException, IOException, ClassNotFoundException
	{
		socket = new Socket("127.0.0.1", 1346);
		
		OutputObject = new ObjectOutputStream(socket.getOutputStream());	
		OutputObject.writeObject(inputX);
	
		InputObject = new ObjectInputStream(socket.getInputStream());		
		double a = (double)InputObject.readObject();
		
		return a;	
	}
}