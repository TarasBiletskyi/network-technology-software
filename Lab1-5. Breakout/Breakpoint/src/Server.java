import java.io.*;
import java.net.*;
import java.sql.*;

public class Server extends Thread{
	private ServerSocket ss;
	
	public Server() throws IOException
	{
		System.out.println("Server started");
		ss = new ServerSocket(1346);
		ss.setSoTimeout(60000);
    }
	
	public  void run()
	{	
		while(true)
		{
			try
			{			
				Socket s = ss.accept();
							
				ObjectInputStream dataInput = new ObjectInputStream (s.getInputStream());
			
				Object inputObj = dataInput.readObject(); 

				ObjectOutputStream dataOutput = new ObjectOutputStream(s.getOutputStream());

				if(inputObj instanceof Double) 
				{ 
					double d = (double)inputObj;
					double returnPosition = GetPaddlePosition(d);
					dataOutput.writeObject(returnPosition);
				}
				else if (inputObj instanceof String) 
				{ 
					String  str = (String)inputObj;
					int BestScore = GetLastBestScore(str);
					dataOutput.writeObject(BestScore);
				} 
				
				else if (inputObj instanceof Integer) 
				{ 
					int  i = (int)inputObj;
					StoreBestScore(i); 
				} 
				
			}
			catch(Exception ex){}
		}
	}
	
	private double GetPaddlePosition(double input)
	{
		return input;
	}
	
	private int GetLastBestScore(String Nick)
	{
		int a = DBConnection.GetBestScore(Nick);
		return a;
	}
	private void StoreBestScore(int Score)
	{
		DBConnection.StoreBestScore(Score);
	}
	
	public static void main(String [] args) throws IOException 
	{
		Thread t = new Server();
		t.start();
	}
}

 class DBConnection {

	static String serverName = "localhost\\sqlexpress";
	static int port = 3306;
	static String user = "Linkoln";
	static String password = "123456";
	static String database = "Breakout";
	static String jdbcUrl = "jdbc:sqlserver://" + serverName + ":" + port + ";user=" + user + ";password=" + password + ";databaseName=" + database +"";
	
	static String PlayerName;
	
	public static int GetBestScore(String NickName)
	{
		int bestScore = 0;
		
		try
		{	
			Boolean t =false;
			
			//Get a connection to db
			Connection myConn = DriverManager.getConnection(jdbcUrl);
			
			//Create a statemen 
			Statement myStmt = myConn.createStatement();
			
			//Execute sql query
			ResultSet myRs = myStmt.executeQuery("select * from Players");
			
			int PlayerScore = 0;
			
			//Process the result set
			while(myRs.next())
			{
				PlayerName = myRs.getString("NickName");
				PlayerScore = myRs.getInt("BestScore");
				
				if(PlayerName.equals(NickName))
				{
					t = true;
					//System.out.println("Founded");
					bestScore = PlayerScore;
					break;
				}
			}	
			
			if(!t) 
			{
				StoreNewPlayer(myConn, NickName, 0);
				PlayerName = NickName;
			}
			
			else System.out.println("NOTStoreNewPlayer" + PlayerName + " " + PlayerScore);
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		
		return bestScore;
	}
	
	public static void StoreBestScore(int Score)
	{	
		System.out.println("StoreBestScore" + Score);
		try
		{	
		  //Get a connection to db
		  Connection myConn = DriverManager.getConnection(jdbcUrl);
					
		  // create the java mysql update preparedstatement
		  String query = "update Players set BestScore = ? where NickName = ?";
		  PreparedStatement preparedStmt = myConn.prepareStatement(query);
		  preparedStmt.setInt   (1, Score);
		  preparedStmt.setString(2, PlayerName);
		
		  // execute the java preparedstatement
		  preparedStmt.executeUpdate();     		
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}

	}
	
	private static void StoreNewPlayer(Connection myConn, String NickName, int BestScore) throws SQLException
	{
		Statement statement = myConn.createStatement();
		System.out.println("StoreNewPlayer");
		// insert the data
		String query = " insert into Players (NickName, BestScore)"+" values (?, ?)";
		 // create the mysql insert preparedstatement
	      PreparedStatement preparedStmt = myConn.prepareStatement(query);
	      preparedStmt.setString (1, NickName);
	      preparedStmt.setInt    (2, BestScore);
	      
	      preparedStmt.execute();
	      
		//statement.executeUpdate("INSERT INTO Players " + "VALUES ("+NickName+"," +BestScore+")");
	}
}
